# Utilise l'image officielle MySQL
FROM mysql:latest

# Définir les variables d'environnement pour la configuration de MySQL
ENV MYSQL_ROOT_PASSWORD=myPassword
ENV MYSQL_DATABASE=mydb

# Exposer le port 3306 pour permettre l'accès externe
EXPOSE 3306
